/**
 * Created by fred on 10/27/16.
 */
window.$ = window.jQuery;
var Cookies = require('js-cookie');

module.exports = function () {

    var defaults = {
        ProspectID: null,
        prospect: {
            firstName: null,
            lastName: null,
            email: null,
            address1: null,
            address2: null,
            city: null,
            state: null,
            zip: null,
            country: "US",
            phone: null
        },
        subscription: null,
        upsell: null
    };

    var data = $.extend(defaults, Cookies.get('customer'));

    return {
        setProspectID(id) {
            data.ProspectID = id;
            return this;
        },
        getProspectID() {
            "use strict";
            return data.ProspectID;
        },
        Shipping: {
            setFirstName(firstName) {
                "use strict";
                data.prospect.firstName = firstName;
                return this;
            },
            getFirstName() {
                "use strict";
                return data.prospect.lastName;
            },
            setLastName(lastName) {
                "use strict";
                data.prospect.lastName = lastName;
                return this;
            },
            getLastName() {
                "use strict";
                return data.prospect.lastName;
            },
            setEmail(email) {
                "use strict";
                data.prospect.email = email;
                return this;
            },
            setAddress1(address1) {
                "use strict";
                data.prospect.address1 = address1;
                return this;
            },
            getAddress1() {
                "use strict";
                return data.prospect.address1;
            },
            setAddress2(address2) {
                "use strict";
                data.prospect.address2 = address2;
                return this;
            },
            getAddress2() {
                "use strict";
                return data.prospect.address2;
            },
            setCity(city) {
                "use strict";
                data.prospect.city = city;
                return this;
            },
            getCity() {
                "use strict";
                return data.prospect.city;
            },
            setState(state) {
                "use strict";
                data.prospect.state = state;
                return this;
            },
            getState() {
                "use strict";
                return data.prospect.state;
            },
            setZip(zip) {
                "use strict";
                data.prospect.zip = zip;
                return this;
            },
            getZip() {
                "use strict";
                return data.prospect.zip;
            },
            setCountry(country) {
                "use strict";
                data.prospect.country = country;
                return this;
            },
            getCountry() {
                "use strict";
                return data.prospect.country;
            },
            setPhone(phone) {
                "use strict";
                data.prospect.phone = phone;
                return this;
            }
        },
        getProspect() {
            "use strict";
            return data.prospect;
        },
        setSubscription(subscription) {
            "use strict";
            data.subscription = subscription;
            return this;
        },
        getSubscription() {
            "use strict";
            return data.subscription;
        },
        setUpsell(upsell) {
            "use strict";
            data.upsell = upsell;
            return this;
        },
        getUpsell() {
            "use strict";
            return data.upsell;
        }
    };
}