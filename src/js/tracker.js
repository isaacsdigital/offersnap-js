/**
 * Created by fred on 10/27/16.
 */

var Cookies    = require('js-cookie');

module.exports = function () {

    inferAttribute('AFFID');
    inferAttribute('SID');
    inferAttribute('CID');
    inferAttribute('clickid');

    function inferAttribute(key) {
        "use strict";
        if (hasParam(key)) {
            Cookies.set(key, getParam(key));
        }
    }

    function hasParam(name) {
        var url     = window.location.search.substring(1);
        var kvPairs = url.split('&');
        for (var i = 0; i < kvPairs.length; i++) {
            var param = kvPairs[i].split('=');
            if (param[0] == name) {
                return true;
            }
        }
        return false;
    }

    function getParam(name) {
        var url     = window.location.search.substring(1);
        var kvPairs = url.split('&');
        for (var i = 0; i < kvPairs.length; i++) {
            var param = kvPairs[i].split('=');
            if (param[0] == name) {
                return param[1];
            }
        }
        return null;
    }


    return {
        switchToUpsellMode() {
            this.isUpsellPage = true;
        },
        getAffiliate() {
            var affid = Cookies.get('AFFID');
            if (this.isUpsellPage) {
                affid = affid + "_2";
            }
            return affid;
        },
        getSubAffiliate() {
            return Cookies.get('SID');
        },
        getCampaign() {
            return Cookies.get('CID');
        },
        getClickId() {
            return Cookies.get('clickid');
        },
        getDataForCharge() {
            "use strict";
            var self = this;
            return {
                affiliate:  self.getAffiliate(),
                subAffiliate: self.getSubAffiliate(),
                campaignID: self.getCampaign()
            }
        }
    };
}