window.$ = window.jQuery;

var Tracker = require('./tracker.js');
var Customer = require('./customer.js');

var SubscriptionBundle = function (subscription, insureship, fallback) {
    "use strict";

    if (subscription === null) {
        throw Error("Subscription.subscription cannot be null");
    }

    return {
        subscription: subscription,
        insureship: insureship,
        fallback: fallback
    }
};

var OfferSnap = function (options) {

    var defaults = {
        baseUrl: "http://offersnap.co",
        site: null
    };

    var config = $.extend(defaults, options);

    console.log("OfferSnap Initialized", this.options);

    return {
        Customer: new Customer(),
        Tracker: new Tracker(),
        getConfig() {
            return config;
        },
        getApiEndpoint(operation) {
            return config.baseUrl + "/api/" + operation;
        },
        saveShipping() {
            var self  = this;
            var url  = self.getApiEndpoint('create-prospect');

            var prospect = self.Customer.getProspect();
            prospect.site = config.site;

            $.extend(prospect, self.Tracker.getDataForCharge());

            return $.post(url, prospect, function (response) {
                if (response.hasOwnProperty("ProspectID")) {
                    self.Customer.setProspectID(response.ProspectID);
                    console.log('Prospect Created', self.Customer.getProspectID());
                }
            });
        },
        createSubscriptionBundle(subscription, insureship, fallback) {
            return new SubscriptionBundle(subscription, insureship, fallback);
        },
        createSubscription(bundle, creditCard, expMonth, expYear, cvv) {

            var url  = this.getApiEndpoint('create-subscription');

            var data = {
                prospect: this.Customer.getProspectID(),
                subscription: bundle.subscription,
                creditCard: creditCard,
                expMonth: expMonth,
                expYear: expYear,
                cvv: cvv
            };

            if (bundle.fallback != null) {
                data.fallback = bundle.fallback;
            }

            if (bundle.insureship != null) {
                data.insureship = bundle.insureship;
            }

            $.extend(data, this.Customer.getProspect(), this.Tracker.getDataForCharge());

            console.log("Subscription Request", data);

            var self = this;
            return $.post(url, data, function (response) {
                "use strict";
                console.log("CreateSubscription:", response);
                if(response.hasOwnProperty("Subscription")) {
                    self.Customer.setSubscription(response);
                }
            });
        },
        createUpsell(bundle) {
            "use strict";

            var url = this.getApiEndpoint("create-upsell");

            var data = {
                prospect: this.Customer.getProspectID(),
                subscription: bundle.subscription,
            }

            if (bundle.fallback != null) {
                data.fallback = bundle.fallback;
            }

            if (bundle.insureship != null) {
                data.insureship = bundle.insureship;
            }

            $.extend(data, this.Tracker.getDataForCharge());

            console.log("Upsell Request", data);

            var self = this;
            return $.post(url, data, function (response) {
                console.log("CreateUpsell", response);
                if(response.hasOwnProperty("Subscription")) {
                    self.Customer.setUpsell(response);
                }
            });
        }
    };
};

window.OfferSnap = OfferSnap;
window.SubscriptionBundle = SubscriptionBundle;